﻿USE m1u2h6;

-- 1 listar los pedidos de los clientes que son de madrid, barcelona o zaragoza
-- esta es la consulta entera sin optimizar
CREATE OR REPLACE VIEW consulta1 AS
SELECT
  c.`CÓDIGO CLIENTE`, p.`NÚMERO DE PEDIDO`, c.EMPRESA, c.POBLACIÓN
  FROM clientes c 
  JOIN pedidos p
   ON c.`CÓDIGO CLIENTE` = p.`CÓDIGO CLIENTE` 
  WHERE c.POBLACIÓN 
  IN ( 'madrid','barcelona','zaragoza');

-- 2 vamos a crear una vista con una consulta optimizada de un trozo de la consulta de arriba
-- se crea la vista lo que pasa esque ya la tenia creada y he puesto otra
  CREATE OR REPLACE VIEW subvista AS
  SELECT
    c.`CÓDIGO CLIENTE`, c.EMPRESA, c.POBLACIÓN 
    FROM clientes c
    WHERE c.POBLACIÓN
    IN ('madrid','barcelona','zaragoza');
    
-- 3 y ahora la implementamos para que no de error en crear una nueva vista con todo el codigo
       CREATE OR REPLACE VIEW optimizada1final AS
  SELECT 
    p.`NÚMERO DE PEDIDO`, s.`CÓDIGO CLIENTE`, s.EMPRESA, s.POBLACIÓN 
  FROM pedidos p 
  JOIN subconsulta1consulta1optimizada s 
  USING(`CÓDIGO CLIENTE`);    

-- 4 mostrar los clientes que han realizado algún pedido en el año 2002 y lo han hecho con tarjeta bancaria ordenado ascendentemente por fecha de pedido
-- esta es la consulta completa sin optimizar


  SELECT
    c.EMPRESA, c.POBLACIÓN, p.`NÚMERO DE PEDIDO`, p.`FECHA DE PEDIDO`, p.`FORMA DE PAGO` 
  FROM clientes c 
  JOIN 
    (
    SELECT
      p.`NÚMERO DE PEDIDO`, p.`FECHA DE PEDIDO`, p.`FORMA DE PAGO` 
  FROM pedidos p 
  WHERE YEAR( p.`FECHA DE PEDIDO`)='2002' 
  AND p.`FORMA DE PAGO`='tarjeta'
      ) s
     USING (`CÓDIGO CLIENTE`) 
  ORDER BY p.`FECHA DE PEDIDO`;


-- ahora hay que optimizar y sacar una vista de la optimizacion


  CREATE OR REPLACE VIEW subvista2 AS
  SELECT p.`NÚMERO DE PEDIDO`, p.`FECHA DE PEDIDO`, p.`FORMA DE PAGO` 
  FROM pedidos p 
  WHERE YEAR( p.`FECHA DE PEDIDO`)='2002' 
  AND p.`FORMA DE PAGO`='tarjeta';


  
-- ahora que tenemos la subvista, la implementamos en la consulta completa para poder hacer la vista con todo el codigo
  SELECT  
  FROM clientes c 
  JOIN subvista2 s 
  USING( `CÓDIGO CLIENTE`) 
  ORDER BY  ;     

-- listar los clientes que han realizado más pedidos
-- consulta1 se calcula los pedidos por cliente
SELECT COUNT(*) npedidos FROM pedidos p GROUP BY p.`CÓDIGO CLIENTE`;

-- consulta2 se calcula que cliente ha hecho el maximo numero de pedidos
SELECT MAX( c1.npedidos) maxpedidos 
  FROM 
  (SELECT COUNT(*) npedidos, p.`CÓDIGO CLIENTE` FROM pedidos p GROUP BY p.`CÓDIGO CLIENTE`) c1;

-- consulta 3 se saca el codigo de cliente del maximo numero de pedidos
 SELECT c1.`CÓDIGO CLIENTE` 
  FROM 
   (SELECT COUNT(*) npedidos, p.`CÓDIGO CLIENTE` FROM pedidos p GROUP BY p.`CÓDIGO CLIENTE`) c1 
      JOIN
 (SELECT MAX( c1.npedidos) maxpedidos FROM 
  (SELECT COUNT(*) npedidos, p.`CÓDIGO CLIENTE` FROM pedidos p GROUP BY p.`CÓDIGO CLIENTE`) c1) c2 
    ON c1.npedidos= c2.maxpedidos;

-- consulta ultima: se saca el telefono y el codigo del cliente con el maximo numero de pedidos
  SELECT c.`CÓDIGO CLIENTE`, c.TELÉFONO FROM (SELECT c1.`CÓDIGO CLIENTE` 
  FROM 
   (SELECT COUNT(*) npedidos, p.`CÓDIGO CLIENTE` FROM pedidos p GROUP BY p.`CÓDIGO CLIENTE`) c1 
      JOIN
 (SELECT MAX( c1.npedidos) maxpedidos FROM 
  (SELECT COUNT(*) npedidos, p.`CÓDIGO CLIENTE` FROM pedidos p GROUP BY p.`CÓDIGO CLIENTE`) c1) c2 
    ON c1.npedidos= c2.maxpedidos) c3 JOIN clientes c ON c.`CÓDIGO CLIENTE`= c3.`CÓDIGO CLIENTE`;
  
  -- crear las vistas
  -- vista1 se saca de la primera consulta
    CREATE OR REPLACE VIEW viscon1 AS
  SELECT COUNT(*) npedidos 
  FROM pedidos p 
  GROUP BY p.`CÓDIGO CLIENTE`;
    
 -- se saca la vista 2 sacando el maximo pedidos con la vista 1
  CREATE OR REPLACE VIEW viscon2 AS
  SELECT MAX(v.npedidos) maximo 
  FROM viscon1 v;
  
-- se saca la vista 3 haciendo un join con la vista 1 y 2 para listar el codigo cliente
  SELECT * FROM viscon1 v JOIN viscon2 v1 ON v= v1;         
